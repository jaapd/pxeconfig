#!/usr/bin/env python
#
# $Id$
#
# set ts=4
#

import sys
import os

from distutils.core import setup, Extension 


setup ( name = 'pxeconfig',
    version = '4.2.0',
    description = 'SARA pxeconfig utilities',
    author = 'Bas van der Vlies',
    author_email = 'pxeconfig@sara.nl',
    url = 'https://oss.trac.sara.nl/oss/pxeconfig',


    extra_path = 'pxeconfig',
    package_dir = { '' : 'src' }, 
    py_modules = [ 'pxeconfig', 'pxeconfigd', 'pxe_global', 'AdvancedParser' ], 
)
